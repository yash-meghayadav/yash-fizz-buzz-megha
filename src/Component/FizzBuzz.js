import React from "react";

class FizzBuzz extends React.Component {
  state = {
    userNumber: [],
    error: ""
  };

  onChangeInput(event) {
    event.preventDefault();
    var dayOfWeek = new Date().getDay();
    var todaysDate = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ][dayOfWeek];

    console.log(todaysDate);

    var no = [];
    if (event.target.value > 1000 || event.target.value < 1) {
      this.setState({
        error: "Input should be in between 1 to 1000"
      });
    } else {
      this.setState({
        error: " "
      });
      for (var i = 1; i <= event.target.value; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
          no.push(<li>{"FizzBuzz"}</li>);
        } else if (i % 5 === 0) {
          no.push(
            <li style={{ color: "Green" }}>
              {todaysDate.charAt(0) + "Buzz".slice(1)}
            </li>
          );
        } else if (i % 3 === 0) {
          no.push(
            <li style={{ color: "Blue" }}>
              {todaysDate.charAt(0) + "Fizz".slice(1)}
            </li>
          );
        } else {
          no.push(i);
        }
      }
    }
    this.setState({ userNumber: [] }, function() {
      this.updateStateForInput(no);
    });
  }

  updateStateForInput(no) {
    console.log("hi");
    this.setState({
      userNumber: Object.assign(this.state.userNumber, no)
    });
  }

  render() {
    return (
      <div>
        <input
          type="number"
          name="number"
          onInput={event => this.onChangeInput(event)}
        />
        <div>
          <ul style={{ listStyleType: "none" }}>
            {this.state.userNumber.map(no => (
              <li>{no}</li>
            ))}
          </ul>
        </div>
        <div>{this.state.error}</div>
      </div>
    );
  }
}

export default FizzBuzz;
