import React from "react";
import "./App.css";

import FizzBuzz from "./Component/FizzBuzz";
function App() {
  return (
    <div className="App">
      <header>Fizz Buzz</header>
      <FizzBuzz />
    </div>
  );
}
export default App;
